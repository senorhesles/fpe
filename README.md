# fpe
file-permissions-edit

I write a lot of one liner scripts. What this entails is that I create a lot of files, make a lot of them executableand edit a lot of them in my text editor.

I did this a couple hundred times until I realized that I could just write a script that did this for me.

I give you: fpe

Download and unzip it. Move it to whatever directory you put your executable files. Make sure that directory is in your PATH.

In Debian Jessie, for example my .bashrc file has...:

	export PATH=$PATH:/sbin:/usr/sbin:$HOME/bin:$HOME

...in it. Make sure to restart your terminal session or source .bashrc...: 

	~$source .bashrc

...to make sure the changes you made are made known to the current terminal session.

The way it works is simple:

	~$fpe testfile.sh

If _testfile.sh_ is in the current user's /bin directory, it will open it in emacs -no-window mode, but if _testfile.sh_ does not exist, fpe will create it, chmod 700 it, and open it in emacs -no-window mode.
